﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParadoxCustomUtilityTools.Common.Helpers
{
    public static class FileHelper
    {
        public static bool FileExists(string path)
        {
            return Directory.Exists(path);
        }
    }
}
